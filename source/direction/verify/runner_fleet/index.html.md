---
layout: markdown_page
title: "Category Direction - Runner Fleet"
description: " Features and capabilities for installing, configuring, managing and monitoring a fleet of GitLab Runners."
canonical_path: "/direction/verify/runner_fleet/"
---

- TOC
  {:toc}

## Runner Fleet

Our vision is to provide users with a birds-eye view, configuration management capabilities, and predictive analytics to administer a fleet of GitLab Runners easily. This category's north star delivers an industry-leading user experience as measured by system usablity scores and the lowest maintenance overhead for customers who self-manage a CI build platform at scale.
## Who we are focusing on?

Check out our [Ops Section Direction "Who's is it for?"](/direction/ops/#who-is-it-for) for an in depth look at the our target personas across Ops. For Runner, our "What's Next & Why" are targeting the following personas, as ranked by priority for support:

1. [Priyanka - Platform Engineer](/handbook/marketing/strategic-marketing/roles-personas/#priyanka-platform-engineer)
2. [Devon - DevOps Engineer](/handbook/marketing/strategic-marketing/roles-personas/#devon-devops-engineer)
3. [Sasha - Software Developer](/handbook/marketing/strategic-marketing/roles-personas/#sasha-software-developer)
4. [Delaney - Development Team Lead](/handbook/marketing/strategic-marketing/roles-personas/#delaney-development-team-lead)

## Strategic Priorities

The table below represents the current strategic priorities for Runner Fleet. This list will change with each monthly revision of this direction page.

|Item|Why?|Target delivery QTR|
|----------|----------------|----------------|
|[Runner Fleet Foundations](https://gitlab.com/groups/gitlab-org/-/epics/6715)|The foundational capabilities are the building blocks required to deliver an improved experience for managing runners at scale.|FY22 Q4|
|[Runner Admin View Usability Improvements](https://gitlab.com/groups/gitlab-org/-/epics/5665)|The 2021 Q3 UX scorecard of the current runner administrative user experience identified several areas that needed improvement regarding workflow and user experience. In conjunction with the foundations' work effort, the usability improvements enable future iterations that will include new features and capabilities.|FY23 Q1|
|[Runner Search, Association and Data Export for Admin and Group views](https://gitlab.com/groups/gitlab-org/-/epics/7181)|Including an assigned group or project data element and adding the ability to export the runner data from GitLab will significantly simplify the operational overhead of locating and troubleshooting runners across an enterprise installation of GitLab.|FY23Q1||
|[Runner Group View Usability Improvements](https://gitlab.com/groups/gitlab-org/-/epics/6714)|The usability improvements described above for the admin view are relevant to the group view. For customers on GitLab SaaS, they use this interface to manage and administer runners that they self-host. Therefore, any usability and feature improvements in the Admin view must also be made available in the Group view.|FY23 Q1|
|[Runner Admin View New Features](https://gitlab.com/groups/gitlab-org/-/epics/6716)|With the foundations in place, we will now be able to deliver new features more quickly. These include summary metrics on all runners associated at the instance, group, or project levels, improved search and filtering, runner association to a project or group, automated removal of stale runners.|FY23 Q2|
|[Runner Group View New Features](https://gitlab.com/groups/gitlab-org/-/epics/5677)|With the foundations in place, we will now be able to deliver new features more quickly. These include improved search and filtering, runner association to a project or group, automated removal of stale runners.|FY23 Q2|

## Maturity Plan

- Runner Fleet is at the ["Viable"](/direction/maturity/) maturity level.
- As detailed in this [epic](https://gitlab.com/groups/gitlab-org/-/epics/6090), we plan to review the maturity scorecard for runner core and complete new category maturity scorecards for the other product development categories, Runner SaaS, and Runner Fleet.

## Give Feedback

The near features highlighted here represent just a subset of the features and capabilities that have been requested by the community and customers. If you have questions about a specific runner feature request or have a requirement that's not yet in our backlog, you can provide feedback or open an issue in the GitLab Runner [repository](https://gitlab.com/gitlab-org/gitlab-runner/-/issues).

## Revision Date

This direction page was revised on: 2022-01-10