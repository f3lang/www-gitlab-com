---
layout: handbook-page-toc
title: "Ideal Customer Profile"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Ideal Customer Profile
Please reference the updated page here (and bookmark for future reference): [/handbook/marketing/account-based-marketing/ideal-customer-profile/](/handbook/marketing/account-based-marketing/ideal-customer-profile/)

### Large

Please reference the updated page here (and bookmark for future reference): [/handbook/marketing/account-based-marketing/ideal-customer-profile/#enterprise-icp](/handbook/marketing/account-based-marketing/ideal-customer-profile/#enterprise-icp)


### Mid Market 

Please reference the updated page here (and bookmark for future reference): [/handbook/marketing/account-based-marketing/ideal-customer-profile/#midmarket-icp](/handbook/marketing/account-based-marketing/ideal-customer-profile/#midmarket-icp)

